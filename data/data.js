export default {
  courses: [
    {
      id: 1,
      title: 'The Elements of AI',
      author: 'Reaktor',
      image: 'https://chiefexecutive.net/wp-content/uploads/2018/02/GettyImages-870184586-compressor.jpg',
      description: 'AI theory and applications',
      lessons: [
        {
          title: 'Chapter 1 - What is AI?',
          startLink: 1,
          segments: [
            {
              id: 1,
              type: 'listen',
              source: {
                type: 'audio',
                file: 'elements-of-ai/1-how-should-we-define-ai.mp3',
                text: `
How should we define AI?
  
In our very first section, we'll become familiar with the concept of AI by looking into its definition and some examples.
  
As you have probably noticed, AI is currently a "hot topic": media coverage and public discussion about AI is almost impossible to avoid. However, you may also have noticed that AI means different things to different people. For some, AI is about artificial life-forms that can surpass human intelligence, and for others, almost any data processing technology can be called AI.
  
To set the scene, so to speak, we'll discuss what AI is, how it can be defined, and what other fields or technologies are closely related. Before we do so, however, we'll highlight three applications of AI that illustrate different aspects of AI. We'll return to each of them throughout the course, to deepen our understanding.
                `
              },
              nextLink: 2,
            },
    
            {
              id: 2,
              type: 'listen',
              source: {
                type: 'audio',
                file: 'elements-of-ai/2-reasons-why-ai-is-hard-to-define.mp3',
              },
              nextLink: 3,
            },
            
            {
              id: 3,
              type: 'listen',
              source: {
                type: 'audio',
                file: 'elements-of-ai/3-defining-ai.mp3',
              },
              nextLink: 4,
            },
          ]
        },

        {
          title: 'Question 1 - Is this AI or not?',
          startLink: 1,
          segments: [

            {
              id: 4,
              type: 'listen',
              source: {
                type: 'audio',
                file: 'elements-of-ai/question-1/question-is-this-ai.mp3',
                text: `
Next up, there will be some examples of things that may or may not be considered AI. You should always think before you answer and then say either 'yes' or 'no' depending if you think it counts as AI.
                `
              },
              nextLink: 5,
            },
            
            {
              id: 5,
              type: 'question',
              answer: 'yes',
              source: {
                type: 'audio',
                file: 'elements-of-ai/question-1/spotify-prompt.mp3',
                text: `
Personalized music recomendations on spotify.
                `
              },
              nextLink: 6,
            },
            
            {
              id: 6,
              type: 'listen',
              source: {
                type: 'audio',
                file: 'elements-of-ai/question-1/spotify-answer.mp3',
                text: `
Yes. The system is learning and adapting to the users listening behavior.
                `
              },
              isSubSegment: true,
              nextLink: 7,
            },

            {
              id: 7,
              type: 'question',
              answer: 'any',
              source: {
                type: 'audio',
                file: 'elements-of-ai/question-1/gps-prompt.mp3',
                text: `
A GPS navigation system for finding the fastest route.
                `
              },
              nextLink: 8,
            },
            
            {
              id: 8,
              type: 'listen',
              source: {
                type: 'audio',
                file: 'elements-of-ai/question-1/gps-answer.mp3',
                text: `
Yes and no. The signal processing and geometry used to determin the coordinates isn't necessarily AI. But providing good suggestions for navigation can be AI especially if variables such as traffic conditions are taken into account.
                `
              },
              isSubSegment: true,
              nextLink: 9,
            },

            {
              id: 9,
              type: 'question',
              answer: 'no',
              source: {
                type: 'audio',
                file: 'elements-of-ai/question-1/photoediting-prompt.mp3',
                text: `
Photo editing features such as brightness and contrast in applications like PhotoShop.
                `
              },
              nextLink: 10,
            },
            
            {
              id: 10,
              type: 'listen',
              source: {
                type: 'audio',
                file: 'elements-of-ai/question-1/photoediting-answer.mp3',
                text: `
Not necessarily. Adjustments such as color balance, contrast and so on, are neither adaptive nor autonomous.
                `
              },
              isSubSegment: true,
              nextLink: null,
            }
          ]
        },
        {
          title: 'Chapter 2 - Related fields',
          startLink: 1,
          segments: [
            {
              id: 1,
              type: 'listen',
              source: {
                type: 'text',
                text: `
                Chapter 2 - Related fields
  
                In addition to AI, there are several other closely related topics that are good to know at least by name. These include machine learning, data science, and deep learning.
                
                Machine learning can be said to be a subfield of AI, which itself is a subfield of computer science. (Such categories are often somewhat imprecise and some parts of machine learning could be equally well or better belong to statistics.) Machine learning enables AI solutions that are adaptive. A concise definition can be given as follows:
                `
              },
              nextLink: 2,
            },
            {
              id: 2,
              type: 'listen',
              source: {
                type: 'text',
                text: `
                Deep learning is a subfield of machine learning, which itself is a subfield of AI, which itself is a subfield of computer science.
                
                We will meet deep learning in some more detail in Chapter 5, but for now let us just note that the “depth” of deep learning refers to the complexity of a mathematical model, and that the increased computing power of modern computers has allowed researchers to increase this complexity to reach levels that appear not only quantitatively but also qualitatively different from before.
                
                (As you notice, science often involves a number of progressively more special subfields, subfields of subfields, and so on. This enables researchers to zoom into a particular topic so that it is possible to catch up with the ever increasing amount of knowledge accrued over the years, and produce new knowledge on the topic — or sometimes, correct earlier knowledge to be more accurate.)
                `
              },
              nextLink: 3,
            },
            {
              id: 3,
              type: 'listen',
              source: {
                type: 'text',
                text: `
                Data science is a recent umbrella term (term that covers several subdisciplines) that includes machine learning and statistics, certain aspects of computer science including algorithms, data storage, and web application development.
                
                Data science is also a practical discipline that requires understanding of the domain in which it is applied in, for example, business or science: its purpose (what "added value" means), basic assumptions, and constraints.
                
                Data science solutions often involve at least a pinch of AI (but usually not as much as one would expect from the headlines).
                `
              },
              nextLink: null,
            },
          ]
        }
      ]
    },
    {
      id: 2,
      title: 'Essentials of Web Development',
      author: 'University of Turku',
      image: 'https://cdn-images-1.medium.com/max/1600/1*HP8l7LMMt7Sh5UoO1T-yLQ.png',
      description: 'Learn the fundamentals of modern Web Development techiniques and the common practicies that surrond the field.',
      lessons: []
    },
    {
      id: 3,
      title: 'Philosophy 101',
      author: 'Harvard University',
      image: 'https://i.kym-cdn.com/photos/images/original/001/153/172/713.jpeg',
      lessons: [],
    },
    {
      id: 4,
      title: 'Cooking with Kate',
      author: 'Kate Goodman',
      image: 'https://usateatsiptrip.files.wordpress.com/2018/03/gettyimages-887636042.jpg?w=1000&h=600&crop=1',
      lessons: [],
    }
  ]
}