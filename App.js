import React from 'react'
import { createStackNavigator, createBottomTabNavigator, createSwitchNavigator, createAppContainer } from "react-navigation"
import Icon from 'react-native-vector-icons/FontAwesome'
import CourseListView from 'src/views/CourseListView'
import CourseView from 'src/views/CourseView'
import ListeningView from 'src/views/ListeningView'
import SettingsView from 'src/views/SettingsView'
import HomeView from 'src/views/HomeView'


const CoursesStack = createStackNavigator(
  {
    CourseListView: {screen: CourseListView},
    CourseView: {screen: CourseView},
    SettingsView: {screen: SettingsView},
  },
  {
    initialRouteName : 'CourseListView',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#EEE',
      },
      header: null
    },
  },
);


const MainTabs = createBottomTabNavigator(
  {
    Home: {
      screen: HomeView,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="home" size={30} color={tintColor} />
        ),
      },
    },
    Courses: {
      screen: CoursesStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="stack-overflow" size={30} color={tintColor} />
        ),
      },
    }
  }, {
    tabBarOptions: {
      activeTintColor: 'orange',
      inactiveTintColor: '#AAA',
      style: {
        height: 55,
        backgroundColor: '#333',
      }
  
    }
  }
)

MainSwitch = createSwitchNavigator(
  {
    Main: {screen: MainTabs},
    ListeningView: {screen: ListeningView},
  }, {
    initialRouteName: 'Main'
  }
)

export default createAppContainer(MainSwitch)