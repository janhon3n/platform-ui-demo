import React, { Component } from "react";
import TextSegmentComponent from "./components/textSegment";
import StartSegmentComponent from "./components/startSegment";
import EndSegmentComponent from "./components/endSegment";
import QuestionSegmentComponent from "./components/questionSegment";
import SegmentComponent from "./components/segment";
import {
  Segment,
  StartSegment,
  EndSegment,
  QuestionSegment,
  TextSegment
} from "./SegmentClasses";

class App extends Component {
  state = {
    i: 2, // running number to give ids
    nodes: [
      {
        segment: new StartSegment(0, [-1], {
          x: 50,
          y: window.innerHeight / 2 - 100
        })
      },
      {
        segment: new EndSegment(1, [], {
          x: window.innerWidth - 350,
          y: window.innerHeight / 2 - 100
        })
      }
    ],
    movingSegment: -1, // -1 = move nothing, -2 = move everything, else move that id
    connectingSegment: { id: -1, index: 0 }, // -1 = not connecting
    mouseOldPos: { x: 0, y: 0 },
    linePos: { x1: 0, x2: 0, y1: 0, y2: 0 },
    mouseDown: false
  };

  // So that the events will fire wherever we click / move
  componentWillMount() {
    document.addEventListener("mousedown", this.handleMouseDown, false);
    document.addEventListener("mousemove", this.handleMouseMove, false);
    document.addEventListener("mouseup", this.handleMouseUp, false);
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleMouseDown, false);
    document.removeEventListener("mousemove", this.handleMouseMove, false);
    document.removeEventListener("mouseup", this.handleMouseUp, false);
  }

  addNode = type => {
    let segment = null;
    let startpos = {
      x: window.innerWidth / 2 - 150,
      y: window.innerHeight / 2 - 50
    };
    switch (type) {
      case "text":
        segment = new TextSegment(this.state.i + 1, [-1], "", startpos);
        break;
      case "question":
        segment = new QuestionSegment(
          this.state.i + 1,
          [-1, -1],
          "",
          ["", ""],
          1,
          startpos
        );
        break;
    }

    const nd = this.state.nodes.concat([
      { id: this.state.i + 1, segment: segment }
    ]);
    this.setState({ i: this.state.i + 1, nodes: nd });
  };

  // TODO are you sure -box
  handleDelete = id => {
    const nd = this.state.nodes.filter(n => n.segment.id !== id);
    nd.forEach(elem => {
      elem.segment.links.map(link => {
        return link == id ? -1 : link;
      });
    });
    this.setState({ i: this.state.i, nodes: nd });
  };

  /*
  Segment moving handlers
  */

  handleSegmentMouseDown = (e, id) => {
    this.setState({
      mouseOldPos: { x: e.pageX, y: e.pageY },
      movingSegment: id
    });
  };

  handleNodeConnectionButtonClick = (e, id, index) => {
    this.setState(state => {
      const nodes = state.nodes.map(elem => {
        if (elem.segment.id === id) {
          elem.segment.links[index] = -1;
        }
        return elem;
      });
      return {
        nodes,
        linePos: { x1: e.pageX, y1: e.pageY, x2: e.pageX, y2: e.pageY }
      };
    });

    this.setState({
      connectingSegment: { id: id, index: index },
      mouseOldPos: { x: e.pageX, y: e.pageY },
      movingSegment: -1
    });
  };

  handleMouseDown = e => {
    this.setState({
      mouseOldPos: { x: e.pageX, y: e.pageY },
      connectingSegment: { id: -1, index: 0 },
      movingSegment: -2,
      upCounter: 0,
      mouseDown: true
    });
  };

  handleMouseMove = e => {
    if (
      this.state.movingSegment !== -1 &&
      this.state.connectingSegment.id === -1
    ) {
      this.setState(state => {
        const nodes = state.nodes.map(elem => {
          if (
            elem.segment.id === this.state.movingSegment ||
            this.state.movingSegment === -2
          ) {
            elem.segment.pos = {
              x: elem.segment.pos.x + e.pageX - state.mouseOldPos.x,
              y: elem.segment.pos.y + e.pageY - state.mouseOldPos.y
            };
          }
          return elem;
        });
        return { nodes, mouseOldPos: { x: e.pageX, y: e.pageY } };
      });
    }
    // Draw the connecting line
    if (this.state.connectingSegment.id > -1) {
      this.setState({
        linePos: {
          x1: this.state.linePos.x1,
          y1: this.state.linePos.y1,
          x2: e.pageX,
          y2: e.pageY
        }
      });
    }
  };

  handleMouseUp = e => {
    this.setState({ movingSegment: -1, mouseDown: false });
  };

  handleSegmentMouseUp = id => {
    if (this.state.connectingSegment.id >= 0) {
      this.setState(state => {
        const nodes = state.nodes.map(elem => {
          if (elem.segment.id === state.connectingSegment.id) {
            elem.segment.links[state.connectingSegment.index] = id;
          }
          return elem;
        });
        return { nodes };
      });
    }
    this.forceUpdate(); // line does not update for some reason, so we have to force it
  };

  updateSegmentData = (id, data) => {
    this.setState(state => {
      const nodes = state.nodes.map(elem => {
        if (elem.id === id) {
          return data;
        }
        return elem;
      });
      return { nodes };
    });
  };

  /*
  Render
  */
  render() {
    document.body.style = "background: grey; height: 100%; overflow: hidden";
    return (
      <div>
        {this.state.nodes.map(node => this.rendSegment(node))}

        <svg
          style={{
            position: "fixed",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
            zIndex: 100,
            pointerEvents: "none"
          }}
        >
          {this.state.connectingSegment.id >= 0 && this.state.mouseDown && (
            <line
              x1={this.state.linePos.x1}
              y1={this.state.linePos.y1}
              x2={this.state.linePos.x2}
              y2={this.state.linePos.y2}
              style={{
                strokeWidth: 2,
                stroke: "white"
              }}
            />
          )}
          {this.state.nodes.map(node =>
            node.segment.links.map((link, i) =>
              this.renderConnectionLine(node, link, i)
            )
          )}
        </svg>

        <div
          style={{
            zIndex: 101,
            position: "fixed",
            left: 0,
            bottom: 0,
            width: "100%",
            backgroundColor: "#111",
            color: "white",
            minHeight: 160
          }}
        >
          Add segment
          <select>
            <option onClick={() => this.addNode("text")}>text to speech</option>
            <option onClick={() => this.addNode("question")}>question</option>
          </select>
          <textarea cols="100" value={JSON.stringify(this.state.nodes)} />
        </div>
      </div>
    );
  }

  // Harcoded offset to position connection line
  // No idea how I could get the positions of thos button elements
  connectionBtnOffsetX(index) {
    return 292;
  }
  connectionBtnOffsetY(index) {
    return 34 + 28 * index;
  }

  renderConnectionLine(node, link, i) {
    if (link < 0) {
      return;
    }
    let n = this.state.nodes.find(x => x.segment.id === link);
    if (n === undefined) {
      return;
    }
    return (
      <line
        key={node.segment.id + "," + i}
        x1={node.segment.pos.x + this.connectionBtnOffsetX(i)}
        y1={node.segment.pos.y + this.connectionBtnOffsetY(i)}
        x2={n.segment.pos.x}
        y2={n.segment.pos.y + 34}
        style={{
          strokeWidth: 2,
          stroke: "white"
        }}
      />
    );
  }

  rendSegment(node) {
    // didn't get switch to work with instanceof...
    if (node.segment instanceof TextSegment) {
      return (
        <TextSegmentComponent
          key={node.segment.id}
          data={node.segment}
          onDelete={this.handleDelete}
          onMouseDown={this.handleSegmentMouseDown}
          onMouseUp={this.handleSegmentMouseUp}
          onConnectionClick={this.handleNodeConnectionButtonClick}
          onDataChanged={this.updateSegmentData}
        />
      );
    } else if (node.segment instanceof QuestionSegment) {
      return (
        <QuestionSegmentComponent
          key={node.segment.id}
          data={node.segment}
          onDelete={this.handleDelete}
          onMouseDown={this.handleSegmentMouseDown}
          onMouseUp={this.handleSegmentMouseUp}
          onConnectionClick={this.handleNodeConnectionButtonClick}
          onDataChanged={this.updateSegmentData}
        />
      );
    } else if (node.segment instanceof StartSegment) {
      return (
        <StartSegmentComponent
          key={node.segment.id}
          data={node.segment}
          onMouseDown={this.handleSegmentMouseDown}
          onMouseUp={this.handleSegmentMouseUp}
          onConnectionClick={this.handleNodeConnectionButtonClick}
        />
      );
    } else if (node.segment instanceof EndSegment) {
      return (
        <EndSegmentComponent
          key={node.segment.id}
          data={node.segment}
          onMouseDown={this.handleSegmentMouseDown}
          onMouseUp={this.handleSegmentMouseUp}
          onConnectionClick={this.handleNodeConnectionButtonClick}
        />
      );
    }
  }
}

export default App;
