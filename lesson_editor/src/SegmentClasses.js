class Segment {
  constructor(id, type, links, pos = { x: 0, y: 0 }) {
    this.id = id;
    this.type = type;
    this.links = links;
    this.pos = pos;
  }
}

class StartSegment extends Segment {
  constructor(id, links, pos = { x: 0, y: 0 }) {
    super(id, "Start", links, pos);
  }
}
class EndSegment extends Segment {
  constructor(id, links, pos = { x: 0, y: 0 }) {
    super(id, "End", links, pos);
  }
}

class TextSegment extends Segment {
  constructor(id, links, text, pos = { x: 0, y: 0 }) {
    super(id, "TextToSpeech", links, pos);
    this.text = text;
  }
}

class QuestionSegment extends Segment {
  constructor(id, links, question, answers, answer, pos = { x: 0, y: 0 }) {
    super(id, "Question", links, pos);
    this.question = question;
    this.answers = answers;
    this.answer = answer;
  }
}

export { Segment, StartSegment, EndSegment, TextSegment, QuestionSegment };
