import React, { Component } from "react";
import SegmentComponent from "./segment";

class EndSegmentComponent extends SegmentComponent {
  content() {
    return (
      <div className="card-body text-white">
        <h5 className="card-title">End</h5>
      </div>
    );
  }

  deleteButton() {
    return;
  }
}

export default EndSegmentComponent;
