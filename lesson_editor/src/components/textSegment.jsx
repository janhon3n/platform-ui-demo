import React, { Component } from "react";
import SegmentComponent from "./segment";

class TextSegmentComponent extends SegmentComponent {
  content() {
    return (
      <div className="card-body text-white">
        <h5 className="card-title">Text to speech</h5>
        <textarea
          rows="2"
          value={this.props.data.text}
          onInput={e => this.updateText(e)}
          onChange={e => this.updateText(e)}
        />
      </div>
    );
  }
  updateText(e) {
    this.props.data.text = e.target.value;
    this.props.onDataChanged(this.props.data, this.props.data.id);
  }
}

export default TextSegmentComponent;
