import React, { Component } from "react";
import SegmentComponent from "./segment";

class QuestionSegmentComponent extends SegmentComponent {
  content() {
    return (
      <div className="card-body text-white">
        <h5 className="card-title">Question</h5>
        <input
          value={this.props.data.question}
          onChange={e => this.updateQuestion(e)}
          onInput={e => this.updateQuestion(e)}
        />
        <div style={{ margin: 5 }}>
          Correct Answer
          <select value={this.props.data.answer}>
            {this.props.data.answers.map((ans, i) => (
              <option
                key={i}
                value={i}
                onClick={() => this.updateCorrectAnswer(i)}
              >
                {i + 1}
              </option>
            ))}
          </select>
        </div>
        {this.props.data.answers.map((answer, i) => this.rendAnswer(answer, i))}
        <div style={{ textAlign: "center", width: "100%", padding: 0 }}>
          <button style={{ marginTop: 10 }} onClick={() => this.addAnswer()}>
            +
          </button>
        </div>
      </div>
    );
  }

  rendAnswer(answer, i) {
    return (
      <div key={i}>
        <h6>Answer {i + 1}</h6>
        <input
          value={answer}
          onInput={e => this.updateAnswer(e, i)}
          onChange={e => this.updateAnswer(e, i)}
        />
        <button
          style={{ margin: 5, float: "right" }}
          onClick={() => this.removeAnswer(i)}
        >
          -
        </button>
      </div>
    );
  }

  renderOption(i) {
    if (this.props.answer === i) {
      return (
        <option key={i} value={i} selected>
          {i + 1}
        </option>
      );
    } else {
      return (
        <option key={i} value={i}>
          {i + 1}
        </option>
      );
    }
  }

  updateAnswer(e, i) {
    this.props.data.answers[i] = e.target.value;
    this.props.onDataChanged(this.props.data, this.props.data.id);
  }

  updateQuestion(e) {
    this.props.data.question = e.target.value;
    this.props.onDataChanged(this.props.data, this.props.data.id);
  }

  updateCorrectAnswer(i) {
    this.props.data.answer = i;
    this.props.onDataChanged(this.props.data, this.props.data.id);
  }

  removeAnswer(i) {
    if (this.props.data.answer === i) {
      this.props.data.answer = 0;
    } else if (this.props.data.answer >= i) {
      this.props.data.answer -= 1; // should probaly use some persistent id rather than index
    }
    this.props.data.answers.splice(i, 1);
    this.props.data.links.splice(i, 1);
    this.props.onDataChanged(this.props.data, this.props.data.id);
    this.forceUpdate();
  }

  addAnswer() {
    this.props.data.answers.push("");
    this.props.data.links.push(-1);
    this.props.onDataChanged(this.props.data, this.props.data.id);
  }
}

export default QuestionSegmentComponent;
