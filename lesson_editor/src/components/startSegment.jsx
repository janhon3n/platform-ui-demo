import React, { Component } from "react";
import SegmentComponent from "./segment";

class StartSegmentComponent extends SegmentComponent {
  content() {
    return (
      <div className="card-body text-white">
        <h5 className="card-title">Start</h5>
      </div>
    );
  }

  deleteButton() {
    return;
  }
}

export default StartSegmentComponent;
