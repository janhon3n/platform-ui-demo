import React, { Component } from "react";

import "./styles.css";

class SegmentComponent extends Component {
  handleMouseDown = e => {
    let r = { pageX: e.pageX, pageY: e.pageY };
    this.props.onMouseDown(r, this.props.data.id);
  };

  handleMouseUp = () => {
    this.props.onMouseUp(this.props.data.id);
  };

  handleConnectionClick = (e, i) => {
    let r = { pageX: e.pageX, pageY: e.pageY };
    this.props.onConnectionClick(r, this.props.data.id, i);
  };

  render() {
    return (
      <div
        className="card-group"
        style={{
          position: "absolute",
          top: this.props.data.pos.y,
          left: this.props.data.pos.x
        }}
        draggable="true"
        onMouseUp={this.handleMouseUp}
        onMouseDown={this.handleMouseDown}
      >
        <div
          className="card border-dark mb-2"
          style={{
            maxWidth: 250,
            minWidth: 250,
            backgroundColor: "#111"
          }}
        >
          <span>{this.content()}</span>
          {this.deleteButton()}
        </div>
        <div
          className="card border-dark mb-2"
          style={{
            maxWidth: 100,
            minWidth: 100,
            backgroundColor: "#111"
          }}
        >
          <div className="card-body text-white">
            {this.props.data.links.map((c, i) => (
              <div key={i}>
                {i + 1}{" "}
                <button
                  className="connectionButton"
                  onMouseDown={e => this.handleConnectionClick(e, i)}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }

  deleteButton() {
    return (
      <button
        style={{
          maxWidth: 100,
          margin: "0px 10px 10px 10px"
        }}
        onClick={() => this.props.onDelete(this.props.data.id)}
      >
        Delete
      </button>
    );
  }

  content() {
    return (
      <div>
        <div className="card-header">Default Segment</div>
        <div className="card-body text-dark">
          <h5 className="card-title">Segment</h5>
          <p className="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
        </div>
      </div>
    );
  }
}

export default SegmentComponent;
