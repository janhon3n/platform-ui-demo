import React from 'react'
import { View, Text } from 'react-native'

export default (props) => {
  return <View>
    <Text style={props.style}>{props.title}</Text>
    {props.children}
  </View>
}