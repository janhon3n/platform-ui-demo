import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Text, View, TouchableHighlight, Image, StyleSheet } from 'react-native'
import Title from 'src/components/Title'

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.02)',
    margin: 5,
    padding: 5,
  },
  title: {
    fontSize: 14,
    textAlign: 'left',
    color: 'white'
  }
})

export default (props) => {
  return <TouchableHighlight
    onPress={() => {
      props.navigate('ListeningView', {
        lessonIndex: props.lessonIndex,
        courseData: props.courseData,
      })
    }} 
    underlayColor={'rgba(0,0,0,0.1)'}>
    
    <View style={styles.container}>
    <Icon
            name="play"
            color='#ddd'
            size={18}
            style={{paddingRight: 10}}
          />
      <Title style={styles.title}>{props.courseData.lessons[props.lessonIndex].title}</Title>
    </View>
  </TouchableHighlight>
}