import React from 'react'
import { View, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'

export default function (props) {
  return (
    <View style={{alignItems: 'center', margin: 30}}>
      <Text style={{ color: 'white', fontSize: 30 }}>
        {"YES "}
        <Icon name="angle-double-right" color="#eee" size={30} />
        <Icon name="angle-double-right" color="#eee" size={30} />
      </Text>
      <Text style={{ color: 'white', fontSize: 30 }}>
        <Icon name="angle-double-left" color="#eee" size={30} />
        <Icon style={{marginRight: 20}} name="angle-double-left" color="#eee" size={30} />
        {" NO"}
      </Text>
    </View>
  )
}