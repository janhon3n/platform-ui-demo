import React from 'react'
import { View , Text, Image, TouchableHighlight } from 'react-native'
import data from 'data/data.js'
import Title from 'src/components/Title'
import Icon from 'react-native-vector-icons/FontAwesome'

class HomeView extends React.Component {
  render () {
    let course = data.courses[0]
    return (
      <View style={{
        flex: 1,
        backgroundColor: '#555',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
      }}>
        <Text style={{color:'#EEE', fontSize: 20, margin: 20, textAlign: 'center'}}>Welcome back!</Text>
        <Text style={{color:'#EEE', textAlign: 'center'}}>{"Continue last played course"}</Text>
        
        <TouchableHighlight
            onPress={() => {
              this.props.navigation.navigate('ListeningView', {
                lessonIndex: 0,
                courseData: course,
              })
            }} 
            underlayColor={'rgba(0,0,0,0.1)'}>
          <View style={{
              flexDirection: 'row',
              margin: 10, 
              backgroundColor: '#FAFAFA',
            }}>
            <Image
              style={{width: 100, height: 100}}
              source={{uri: course.image}}
            />
            <View style={{padding: 10}}>
              <Title fontSize={20}>{course.title}</Title>
              <Text>{"By " + course.author}</Text>
              <Text>{course.lessons[0].title}</Text>
            </View>
          </View>
          </TouchableHighlight>
      </View>
    )
  }
}

export default HomeView