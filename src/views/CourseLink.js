import React from 'react'
import { Text, View, TouchableHighlight, Image, StyleSheet } from 'react-native'
import Title from 'src/components/Title'

const styles=StyleSheet.create({
  card: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#575757',
    margin: 6,
    padding: 5,
    paddingLeft: 20,
    borderRadius: 30,
    borderWidth: 1,
  },
  title: {
    fontSize: 18,
    alignSelf: 'flex-start',
    color: 'white',
  },
  text: {
    color: 'white',
    fontStyle: 'italic',
    fontSize: 14,
    opacity: 0.7,
  },
  image: {
    width: 60, 
    height: 60,
    paddingRight: 15,
    borderRadius: 10
  }
})

export default (props) => {
  let data = props.courseData
  return <TouchableHighlight
    onPress={() => {
      props.navigate('CourseView', {
        courseData: data,
      })
    }} 
    underlayColor={'rgba(0,0,0,0.1)'}>
    
    <View style={styles.card}>
      <Image
        source={{uri: data.image}} style={styles.image} />
      <View style={{
        flex:1,
        padding: 12,
        alignItems: 'flex-start',
      }}>
        <Text style={styles.text}>{data.author}</Text>
        <Title style={styles.title}>{data.title}</Title>
        
      </View>
    </View>
  </TouchableHighlight>
}