import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function ListeningControlInfo(props) {
    return (
      <View style={{padding: 10}}>
        <Text style={styles.text}>A or YES - Swipe right</Text>
        <Text style={styles.text}>B or NO - Swipe left</Text>
      </View>
    )
}

const styles = StyleSheet.create({
  text: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center',
  }
});