import React from 'react'
import { View, Image, Dimensions, Text, StyleSheet } from 'react-native'
import Title from 'src/components/Title'
import ListGroup from 'src/components/ListGroup';
import LessonLink from './LessonLink'
import { ScrollView } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#303030',
    flex: 1
  },
  title: {
    fontSize: 30,
    color: 'white',
    padding: 5,
    fontWeight: 'bold'
  },
  author: {
    color: 'white',
    fontStyle: 'italic',
    fontSize: 18,
    opacity: 0.7,
    marginLeft: 3,
    paddingBottom: 5
  },
  description: {
    color: 'white',
    padding: 5
  },
  image: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width,
  },
  listGroupTitle: {
    textAlign: 'left',
    fontSize: 16,
    padding: 5,
    alignSelf: 'stretch',
    color: 'white',
  }
})

export default (props) => {
  let navigate = props.navigation.navigate
  let courseData = props.navigation.getParam('courseData')
  return <ScrollView style={styles.container}>
      <Image
          source={{uri: courseData.image}} style={styles.image} />
      <View style={{ marginLeft: 20 }}>
        <Title style={styles.title}>{courseData.title} <Text style={styles.author}>{"by " + courseData.author}</Text></Title>
        <View style={{height: 3, alignSelf: 'stretch', backgroundColor: '#dddddd'}}/>
        <View style={{marginLeft: 10}}>
          <Text style={styles.description}>{courseData.description}</Text>
          <View style={{height: 2, alignSelf: 'stretch', backgroundColor: '#dddddd'}}/>
          <ListGroup style={styles.listGroupTitle} title="Lessons">
              {courseData.lessons.map((lesson, lessonIndex) => {
                return <LessonLink
                  key={lesson.title}
                  navigate={navigate}
                  lessonIndex={lessonIndex}
                  courseData={courseData} />
              })}
          </ListGroup>
        </View>
      </View>
  </ScrollView>
}