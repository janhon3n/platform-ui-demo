import React from 'react'
import { Text, View, Alert, BackHandler } from 'react-native'
import GestureRecognizer from 'react-native-swipe-gestures'
import LessonPlayer from 'src/classes/LessonPlayer'
import ListeningControls from './ListeningControls'
import ListeningViewText from './ListeningViewText'
import Title from '../components/Title'
import SegmentProgressMarkers from './SegmentProgressMarkers'
import ListeningViewAnswers from './ListeningViewAnswers';

export default class ListeningView extends React.Component {

  constructor(props) {
    super(props)

    this.handleBackPress = this.handleBackPress.bind(this)
    this.navigateToLesson = this.navigateToLesson.bind(this)
    this.userFeedback = this.userFeedback.bind(this)
    this.startLesson = this.startLesson.bind(this)

    this.state = {
      currentLessonIndex: null,
      lessonPlayerState: null,
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    let lessonIndex = this.props.navigation.getParam('lessonIndex')
    this.startLesson(lessonIndex)
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
    this.lessonPlayer && this.lessonPlayer.stop()
  }

  handleBackPress() {
    this.props.navigation.navigate('Main')
    return true
  }

  userFeedback(answer) {
    this.lessonPlayer.reportUserFeedback(answer)
  }

  navigateToLesson(newLessonIndex) {
    this.lessonPlayer && this.lessonPlayer.stop()

    let courseData = this.props.navigation.getParam('courseData')
    console.log('navigating to lesson ' + newLessonIndex)
    if (newLessonIndex < 0 || newLessonIndex >= courseData.lessons.length) {
      this.props.navigation.navigate('Main')
    } else {
      this.startLesson(newLessonIndex)
    }
  }

  startLesson(lessonIndex) {
    let courseData = this.props.navigation.getParam('courseData')
    if (courseData.lessons[lessonIndex].segments == null || 
      courseData.lessons[lessonIndex].segments.length == 0){
        this.props.navigation.navigate('Main')
        Alert.alert("Lesson is empty");
        return;
      }
      
    this.lessonPlayer = new LessonPlayer(courseData.lessons[lessonIndex])
    this.lessonPlayer.stateLinkingFunction = (state) => {
      this.setState({ lessonPlayerState: state })
    }
    this.lessonPlayer.navigationLinkingFunction = (event) => {
      if (event === 'prev') this.navigateToLesson(lessonIndex - 1)
      if (event === 'next') this.navigateToLesson(lessonIndex + 1)
    }
    this.lessonPlayer.play()
    this.setState({ currentLessonIndex: lessonIndex })
  }

  render() {
    let { lessonPlayerState, currentLessonIndex } = this.state
    let courseData = this.props.navigation.getParam('courseData')
    let lessonData = currentLessonIndex !== null ? courseData.lessons[currentLessonIndex] : null
    let segmentData = (lessonData && lessonPlayerState) ? lessonData.segments[lessonPlayerState.segmentIndex] : null

    return <View
      style={{
        flex: 1,
        backgroundColor: '#111',
        paddingTop: 30,
      }}>
      {
        !lessonPlayerState ?
          <Text>Loading...</Text> :
          <GestureRecognizer
            style={{
              flex: 1,
              justifyContent: 'space-between',
            }}
            onSwipeLeft={() => this.userFeedback('no')}
            onSwipeRight={() => this.userFeedback('yes')}
          >
            <View>
              <Title fontSize={16} color='white'>{courseData.title}</Title>
              <Title fontSize={20} color='white'>{lessonData && lessonData.title}</Title>
              <SegmentProgressMarkers
                segments={lessonData.segments}
                currentSegment={lessonPlayerState.segmentIndex}
                questionAnswers={lessonPlayerState.answers}
              />
            </View>
            <ListeningViewText segmentData={segmentData} />
            {segmentData && segmentData.type === 'question' && <ListeningViewAnswers />}
            <ListeningControls
              playState={lessonPlayerState.playState}
              onPlay={this.lessonPlayer.play}
              onPause={this.lessonPlayer.pause}
              onNextSegment={this.lessonPlayer.nextSegment}
              onPrevSegment={this.lessonPlayer.prevSegment}
            />
          </GestureRecognizer>
      }
    </View>
  }
}