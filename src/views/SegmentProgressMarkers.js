import React from 'react'
import { View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'

export default function (props) {
  let {segments, currentSegment, questionAnswers} = props
  return (
    <View style={{margin: 5, flexDirection: 'row', justifyContent: 'center', alignItems:'center'}}>
      {
        segments.map((segment, i) => {
          if (segment.isSubSegment) return null
          let iconType = getIconType(questionAnswers, segment, i)
          let color = getIconColor(questionAnswers, currentSegment, segment, i)
          let size = segment.question ? 18 : 16
          return <Icon size={size} style={{margin: 3}} key={i} name={iconType} color={color} />
        })
      }
    </View>
  )
}

function getIconType(questionAnswers, segment, i) {
  return segment.type === 'question' ?
    (questionAnswers[i] !== null ?
      (answerIsCorrect(segment.answer, questionAnswers[i]) ? 'check' : 'close')
      : 'question')
    : 'circle'
}

function getIconColor(questionAnswers, currentSegment, segment, i) {
  if(i === currentSegment) return '#5697ff'
  return questionAnswers[i] !== null ?
    (answerIsCorrect(segment.answer, questionAnswers[i]) ? '#179b00' : '#ad0000')
    : (i < currentSegment ? '#EEE' : '#555')
}


function answerIsCorrect(correctAnswer, userAnswer) {
  return (
    correctAnswer === 'any' ||
    correctAnswer === userAnswer
  )
}