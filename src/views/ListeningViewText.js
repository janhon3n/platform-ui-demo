import React from 'react'
import {ScrollView, Text} from 'react-native'

export default function (props) {
  if (!props.segmentData) return <ScrollView />
  if (!props.segmentData.source.text) return <ScrollView />

  return (
    <ScrollView>
      <Text style={{
        backgroundColor: '#222',
        color: '#eee',
        margin: 10,
        padding: 10,
        fontSize: (props.segmentData.type === 'question' ? 20 : 16)
      }}>
        {props.segmentData.source.text}
      </Text>
    </ScrollView>
  )
}