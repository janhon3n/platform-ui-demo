import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import { View, Text, TouchableHighlight, StyleSheet } from 'react-native'
import {SearchBar} from 'react-native-elements'
import Title from 'src/components/Title'
import ListGroup from 'src/components/ListGroup'
import CourseLink from './CourseLink';

import data from 'data/data'

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#303030',
    flex: 1
  },
  title: {
    alignSelf: 'stretch',
    textAlign: 'left',
    fontSize: 30,
    padding: 20,
    color: 'white',
    backgroundColor: '#3E50B4'
  },
  listGroupTitle: {
    textAlign: 'center',
    fontSize: 16,
    padding: 5,
    alignSelf: 'stretch',
    color: 'white'
  }
})

export default class Learn extends React.Component {

  state = {
    search: '',
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <TouchableHighlight
        underlayColor="#AAA"
        style={{margin: 5, padding: 5}}
          onPress={() => navigation.navigate('SettingsView')} >
          <Icon
            name="cog"
            color="#555"
            size={30}
          />
        </TouchableHighlight>
      ),
    }
  }

  updateSearch = search => {
    this.setState({ search })
  }

  render() {
    let navigate = this.props.navigation.navigate
    const { search } = this.state
    return (
      <View style={styles.container}>
        <Title style={styles.title}>Eudio</Title>
        <SearchBar
          placeholder="Search"
          onChangeText={this.updateSearch}
          value={search}
          lightTheme={false}/>
        <ListGroup style={styles.listGroupTitle} title="Featured Courses">
          {data.courses.filter(course => {
            let regExp = new RegExp(search, "i")
            return course.title.search(regExp) !== -1 ||
              (course.description && course.description.search(regExp) !== -1) ||
              (course.author && course.author.search(regExp) !== -1)
          }).map(course => {
            return <CourseLink
              key={course.id}
              navigate={navigate}
              courseData={course} />
          })}
        </ListGroup>
      </View>
    );
  }
}