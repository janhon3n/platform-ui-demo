import React from 'react'
import {View} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import {PlayState} from '../classes/LessonPlayer'

export default function ListeningControls(props) {
  let {playState, onPlay, onPause, onNextSegment, onPrevSegment} = props
  return <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
    <Icon onPress={onPrevSegment} style={{ margin: 10 }} name="backward" color="white" size={40} />
    {
      playState === PlayState.PLAYING ?
        <Icon onPress={onPause} style={{ margin: 10 }} name="pause" color="white" size={60} /> :
        <Icon onPress={onPlay} style={{ margin: 10 }} name="play" color="white" size={60} />
    }
    <Icon onPress={onNextSegment} style={{ margin: 10 }} name="forward" color="white" size={40} />
  </View>
}