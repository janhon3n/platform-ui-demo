import React from 'react'
import Tts from 'react-native-tts'
import { View, Text, Picker, AsyncStorage } from 'react-native'

class SettingsView extends React.Component {

  static navigationOptions = {
    title: 'Settings',
  };

  constructor(props) {
    super(props)

    this.state = {
      ttsVoice: null,
      ttsVoices: []
    }
  }

  async componentDidMount() {
    let voices = await Tts.voices()
    let voice = await AsyncStorage.getItem('@Settings:TTS_VOICE')
    this.setState({ttsVoices: voices, ttsVoice: voice})
  }

  async componentWillUnmount() {
    await AsyncStorage.setItem('@Settings:TTS_VOICE', this.state.ttsVoice)
  }

  render() {
    return (
      <View style={{padding: 20}}>
        <Text>Text to Speech voice</Text>
        <Picker
          selectedValue={this.state.ttsVoice}
          onValueChange={(value) => {
            Tts.setDefaultVoice(value)
            this.setState({ttsVoice: value})
          }}>
          {
            this.state.ttsVoices.map(v => {
              return <Picker.Item label={v.id} value={v.id} />
            })
          }
        </Picker>
      </View>

    )
  }
}

export default SettingsView