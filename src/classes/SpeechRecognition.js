import Voice from 'react-native-voice'

export default class SpeechRecognition {
	constructor(lessonPlayer) {
    this.lessonPlayer = lessonPlayer

		Voice.onSpeechStart = this.onSpeechStart.bind(this)
		Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this)
    Voice.onSpeechResults = this.onSpeechResults.bind(this) 

    this.validAnswers = {
      yes: ['yeah', 'yes', 'sure', 'yea', 'absolutely', 'totally'],
      no: ['no', 'nah', 'nope', 'no way', 'hell no', 'never']
    }
  }

  onSpeechStart(e) {
    console.log('Speech recognition started')
  }
  onSpeechRecognized(e) {
    console.log('Speech recognized!')
  }
  onSpeechResults(e) {
    console.log('Speech results', e.value)
    let res = this.validateSpeech(e.value)
    if (res) {
      this.lessonPlayer.reportUserFeedback(res)
    }
  }

  async startRecognition(e) {
    await Voice.start('en-US')
  }

  isRecognizing() {
    return Voice.isRecognizing()
  }

  async stopRecognition() {
    try {
      console.log('Stopping voice recognition')
      await Voice.stop()
    } catch (e) {
      console.error(e)
    }
  }

  validateSpeech(speech) {
    for (const key of Object.keys(this.validAnswers)) {
      if (this.validAnswers[key].indexOf(speech[0].toLowerCase()) >= 0) {
        return key
      }
    }
    return false
  }
}