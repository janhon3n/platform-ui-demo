import Tts from 'react-native-tts'
import { AsyncStorage } from 'react-native'

AsyncStorage.getItem('@Settings:TTS_VOICE', (error, voice) => Tts.setDefaultVoice(voice))
Tts.setDefaultLanguage('en-US')
Tts.setDefaultRate(0.45)

/*
Speech and other playable segments implement the following interface

play() -> Promise<>
  - resolve on playback ended successfully
  - reject when playback interrupted by calling stop()

stop() -> void
  - stops the playback and rejects the promise of the play() function

*/


export default class Speech {
  constructor(text) {
    this.text = text
    this.speechFinished = this.speechFinished.bind(this)
    Tts.addEventListener('tts-finish', this.speechFinished);
  }

  play() {
    let promiseResolve
    let promiseReject
    let promise = new Promise((resolve, reject) => {
      promiseResolve = resolve
      promiseReject = reject
    })

    this.speechFinishedResolve = promiseResolve
    this.speechInterruptedReject = promiseReject

    Tts.speak(this.text);
    return promise
  }

  speechFinished() {
    if (this.speechFinishedResolve) {
      this.speechFinishedResolve()
      this.speechFinishedResolve = null
      this.speechInterruptedReject = null
    }
  }

  stop() {
    if (this.speechInterruptedReject) {
      this.speechInterruptedReject()
      this.speechFinishedResolve = null
      this.speechInterruptedReject = null
    }
    Tts.stop()
  }
}