import Sound from 'react-native-sound'

const assetsUrl = 'http://35.228.142.84/'

export default class Audio {
  constructor(source) {
    this.source = source
  }

  init() {
    let audio
    let sourceFile = this.source.file

    let promise = new Promise((resolve, reject) => {
      console.log('Loading audio from: ' + assetsUrl + sourceFile)
      audio = new Sound(assetsUrl + sourceFile, null, (err) => {
        if (err) {
          console.warn('sound loading failed: ', err)
          reject(err)
        } else {
          resolve()
        }
      })
    })

    this.audio = audio
    return promise
  }


  play() {  
    let audio = this.audio
    let playbackInterruptedReject
    let promise = new Promise((resolve, reject) => {
      playbackInterruptedReject = reject
      audio.play(success => {
          resolve()
      })
    })
    this.playbackInterruptedReject = playbackInterruptedReject
    return promise
  }

  
  stop() {
    this.audio.stop()
    if (this.playbackInterruptedReject)
      this.playbackInterruptedReject(new Error('Audio playback was interrupted'))
  }
}