import { Vibration } from 'react-native'
import Audio from './Audio'
import Speech from './Speech'
import SpeechRecognition from 'src/classes/SpeechRecognition'

export const PlayState = Object.freeze({ "STOPPED": 1, "PLAYING": 2, "PAUSED": 3 })

export default class LessonPlayer {

  currentlyPlayingSegment = null

  constructor(lesson) {
    this.lesson = lesson

    this.play = this.play.bind(this)
    this.stop = this.stop.bind(this)
    this.pause = this.pause.bind(this)
    this.nextSegment = this.nextSegment.bind(this)
    this.prevSegment = this.prevSegment.bind(this)
    this.nextLesson = this.nextLesson.bind(this)
    this.prevLesson = this.prevLesson.bind(this)

    this.reportUserFeedback = this.reportUserFeedback.bind(this)
    this.playSegment = this.playSegment.bind(this)
    this.speechRecognition = new SpeechRecognition(this)

    this.state = {
      playState: PlayState.STOPPED,
      segmentIndex: 0,
      answers: lesson.segments.map(() => {
        return null
      })
    }
  }

  async playSegment(segment) {
    console.log('Playing segment ' + segment.id)
    this.currentlyPlayingSegment = await this.createPlayableSegment(segment)
    await this.currentlyPlayingSegment.play()

    if (segment.type === 'question') {
      await this.handleQuestionAnswer(segment)
    }
  }

  async createPlayableSegment(segmentData) {
    var segment
    if (segmentData.source.type === 'audio') {
      segment = new Audio(segmentData.source)
      await segment.init()
    } else if (segmentData.source.type === 'text') {
      segment = new Speech(segmentData.source.text)
    } else {
      throw new Error('Unknown segment type')
    }
    return segment
  }

  async handleQuestionAnswer(segmentData) {
    this.speechRecognition.startRecognition()
    let answer = await this.waitForUserFeedback()
    let answers = [...this.state.answers]
    answers[this.state.segmentIndex] = answer
    this.setState({answers})
    Vibration.vibrate(100)
    this.speechRecognition.stopRecognition()
  }

  waitForUserFeedback() {
    let promiseResolve
    let promise = new Promise((resolve, reject) => {
      promiseResolve = resolve
    })
    this.resolveUserFeedback = promiseResolve
    return promise
  }

  reportUserFeedback(feedback) {
    if (this.resolveUserFeedback) {
      this.resolveUserFeedback(feedback)
      this.resolveUserFeedback = null
    }
  }

  stopCurrentSegment() {
    if (this.currentlyPlayingSegment) this.currentlyPlayingSegment.stop()
    this.reportUserFeedback(null)
    if (this.speechRecognition.isRecognizing()) this.speechRecognition.stopRecognition()
  }


  // playback controls
  async play() {
    if (this.state.playState !== PlayState.PLAYING) {
      this.setState({ playState: PlayState.PLAYING })
      while (this.state.playState === PlayState.PLAYING
        && this.state.segmentIndex >= 0
        && this.state.segmentIndex < this.lesson.segments.length) {
        try {
          await this.playSegment(this.lesson.segments[this.state.segmentIndex])

          // playback was successful
          this.setState({ segmentIndex: this.state.segmentIndex + 1 })
        } catch(err) {
          // playback was interrupted
          console.log(err.message)
        }
      }
      //playing stopped
      this.speechRecognition.stopRecognition()
    }

    if (this.state.segmentIndex < 0 && this.navigationLinkingFunction)
      this.navigationLinkingFunction && this.navigationLinkingFunction('prev')

    if (this.state.segmentIndex >= this.lesson.segments.length && this.navigationLinkingFunction)
      this.navigationLinkingFunction && this.navigationLinkingFunction('next')
  }

  stop() {
    this.setState({ playState: PlayState.STOPPED, segmentIndex: 0 })
    this.stopCurrentSegment()
  }

  pause() {
    this.setState({ playState: PlayState.PAUSED })
    this.stopCurrentSegment()
  }

  nextSegment() {
    if (this.state.segmentIndex === this.lesson.segments.length - 1) {
      this.nextLesson()
      return
    }
    this.setState({ segmentIndex: this.state.segmentIndex + 1 })
    this.stopCurrentSegment()
  }

  prevSegment() {
    if (this.state.segmentIndex === 0) {
      this.prevLesson()
      return
    }
    this.setState({ segmentIndex: Math.max(0, this.state.segmentIndex - 1) })
    this.stopCurrentSegment()
  }

  nextLesson() {
    this.navigationLinkingFunction && this.navigationLinkingFunction('next')
  }

  prevLesson() {
    this.navigationLinkingFunction && this.navigationLinkingFunction('prev')
  }


  setState(update) {
    this.state = Object.assign({}, this.state, update)
    if (this.stateLinkingFunction) {
      this.stateLinkingFunction(this.state)
    }
  }
}
